#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 28 05:03:35 2018

@author: Miłosz Martynow
"""
import sys 
import datetime as dt

import requests
from requests import ConnectionError 

import json
import csv

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.cm as cm

class ReadJsonData:

    def __init__(self, default_URL, default_file_name):        
        self.default_URL = default_URL
        self.default_file_name = default_file_name

    # JSON import part
    def import_json_from_internet(self, URL=None):

        if URL == None:
            URL = self.default_URL
        else:
            URL = URL
            
        try:
            main_api = URL#'https://api.spacexdata.com/v2/launches'
            main_api = main_api + '?'
            json_data = requests.get(main_api).json()

        except ConnectionError:        
            print('There is no internet?!')
            sys.exit(0)

        return json_data
    
    def import_json_from_file(self, json_name=None):
        
        if json_name == None:
            json_name = self.default_file_name
        else:
            json_name = json_name

        try:
            with open(json_name, 'r') as jd:
                json_data = json.load(jd)
                
        except FileNotFoundError:
            print("Where is JSON?")
            
        except ValueError:
            print("JSON file is incorrect.")
                
        return json_data
    
    def load_data(self, URL=None, json_name=None):
        
        if URL == None:
            URL = self.default_URL
        else:
            URL = URL
        
        if json_name == None:
            json_name = self.default_file_name
        else:
            json_name = json_name
        
        try:
            json_data = self.import_json_from_internet(URL=URL)
            print('Newest JSON data file was loded from:')
            print(URL)
            
        except SystemExit:
            print('Check for file (default_file_name) inside working directry.')
            json_data = self.import_json_from_file(json_name=json_name)
            print(json_name, 'was loaded.')
            
        return json_data

class ReadJson:
        
    def __init__(self, json_file, path_to_content):
        
        self.json_file = json_file
        self.path_to_content = path_to_content
    
    ### PRE-PROCESSING
    
    # JSON processing part
    def make_base_json(self, json_file=None, path_to_content=None):
        
        if json_file == None:
            json_file = self.json_file
        else:
            json_file = json_file

        if path_to_content == None:
            path_to_content = self.path_to_content
        else:
            path_to_content = path_to_content

        data = []
        
        len_of_path = len(path_to_content)
        if len(json_file) == 0:
            base_json = json_file
        else:
            base_json = json_file[0]
            
        
        # if there is only one element in path
        if len_of_path == 1:
            data = base_json[path_to_content[0]]
        
        # if there is more than one element in path
        if len_of_path > 1:
            
            # is there any empty list or marker
            empty_list_exist = bool()
            empty_list_exist = [] in path_to_content
            
            # in case of path without empty list or marker
            if empty_list_exist == False:
                
                for i in range(len_of_path):
                    base_json = base_json[path_to_content[i]]
                data = base_json
                
            # in case of path with empty list or marker
            if empty_list_exist == True:
                
                # update base_json to the marker
                marker = path_to_content.index([])
                for i in range(marker):
                    base_json = base_json[path_to_content[i]]
                    
                for i in range(len(base_json)):
                    base_json_i = base_json[i]
                    
                    # update_base_json by path elements after marker
                    for j in range(marker+1,len_of_path):
                        base_json_j = base_json_i[path_to_content[j]]
                    
                    data.append(base_json_j)
        
        return data   

    # make raw list of lists of all interesting data
    def read_json_files_as_a_list(self, json_file=None, path_to_content=None):
        
        if json_file == None:
            json_file = self.json_file
        else:
            json_file = json_file

        if path_to_content == None:
            path_to_content = self.path_to_content
        else:
            path_to_content = path_to_content

        data_list = []
        for i in range(len(json_file)):
            
            data_line = []
            for j in range(len(path_to_content)):
                
                data = self.make_base_json(json_file=[json_file[i]], path_to_content=path_to_content[j])
                data_line.append(data)
            
            data_list.append(data_line)
        
        return data_list

    ### IN-PROCESSING
    
    # filter JSON by sucessful start criterium 
    def filter_by_sucessful_start(self, data=None, column=None):
        
        if data == None:
            data = self.read_json_files_as_a_list()
        else:
            data = data
            
        if column == None:
            column = 2
        else:
            column = column
            
        ups = 0
        data_list = []
        for data_line in data:
            if data_line[column] == True:
                data_list.append(data_line)
            else:
                ups += 1
        
        print()
        print('There was ', ups, ' unsuccessful missions.')
        print('Clear')
        
        return data_list
    
    # filter JSON by incomplete mass information criterium 
    def filter_by_full_mass_information(self, data=None, column=None):
        
        if data == None:
            data = self.read_json_files_as_a_list()
        else:
            data = data
            
        if column == None:
            column = len(data[0])-1
        else:
            column = column
        
        ups = 0
        data_list = []
        for data_line in data:
            if None in data_line[column]:
                ups += 1
            else:
                data_list.append(data_line)
                
        print()
        print('There was ', ups, ' missions with incomplete mass information.')
        print('Clear')
        
        return data_list        
    
    # find total mass of cargo
    def cargo_mass(self, data=None, column=None):

        if data == None:
            data = self.read_json_files_as_a_list()
        else:
            data = data
            
        if column == None:
            column = len(data[0])-1
        else:
            column = column
        
        for i in range(len(data)):
            data[i][column] = sum(data[i][column])
            
        return data
    
    # drop the column from matrix-like list of lists
    def drop_the_colum(self, data=None, column=None):
        
        if data == None:
            data = self.read_json_files_as_a_list()
        else:
            data = data
            
        if column == None:
            column = 0
        else:
            column = column
            
        for data_line in data:
            del data_line[column]
        
        return data
    
    # extract the column from matrix-like list of lists
    def extract_the_column(self, data=None, column=None):
        
        if data == None:
            data = self.read_json_files_as_a_list()
        else:
            data = data
            
        if column == None:
            column = 0
        else:
            column = column
        
        col = []
        for data_line in data:
            col.append(data_line[column])
        
        return col
    
    # convert unix to datetime
    def convert_unix(self, data=None, column=None):

        if data == None:
            data = self.read_json_files_as_a_list()
        else:
            data = data
            
        if column == None:
            column = 0
        else:
            column = column
            
        for i in range(len(data)):
            data[i][column] = dt.datetime.fromtimestamp(data[i][column]).strftime('%Y-%m-%d %H:%M:%S')
        
        return data

    # save as csv
    def save_as_csv(self, name=None, data=None, labels=None):
        
        if name == None:
            name = 'data.csv'
        else:
            name = name
        
        if data == None:
            data = self.read_json_files_as_a_list()
        else:
            data = data
        
        if labels == None:
            labels = False
        else:
            labels=labels
      
        with open(name, 'w', newline='') as file:
            writer = csv.writer(file)
            
            if labels != False:
                writer.writerow(labels)
            
            for data_line in data:
                writer.writerow(data_line)
        
        print()
        print('Extracted data saved as: ', name)
        
    
    ### POST-PROCESSING

    # Library part
    def check_important_libraries(self, list_of_libraries = None):
        
        print()
        print('Check for python 3.* non-standard libraries')
        print('docs.python.org/3/library/')
        print()
        
        if list_of_libraries == None:
            libraries_list = ['numpy', 'matplotlib']
        else:
            libraries_list = list_of_libraries
            
        for lib in libraries_list:
            
            lib_bool = []
            if lib in sys.modules:
                print('...', lib, 'is installed.')
                lib_bool.append(True)    
                
            else:
                print('Please, install the ', lib, ' library.')
                print('Unless, you dont want to plot the results.')
                lib_bool.append(False)
            
        if all(lib == True for lib in lib_bool):
            return True
        else:
            return False
    
    # check ability to plot 
    def plot_ability(self):
        
        plot_ability = self.check_important_libraries()
        if plot_ability == True:
            print()
            print('Libraries OK!')
            print('Plot will saved in work folder')
        else:
            print()
            print('There will be no plot.')
            print('Check for additional libraries.')
            sys.exit(0)
            
    def sort_rockets(self, data, column):
        
        rocket_column = column
        
        names = []
        for i in range(len(data)):
            names.append(data[i][rocket_column])
            
        names_types = list(set([rocket for rocket in names if names.count(rocket) >= 1]))
        number_of_rocktes = len(names_types)
        
        rockets = []
        for i in range(number_of_rocktes):
            
            rocket = []
            for j in range(len(data)):
                
                if data[j][rocket_column] == names_types[i]:
                    rocket.append(data[j])
                    
            rockets.append(rocket)
        
        return names_types, number_of_rocktes, rockets 

    def colors_and_symbols(self, number_of_rocktes=None, data=None, column=None):
        
        if number_of_rocktes == None:
            print('Set data file and column as an function argument if you dont want give number_of_rocket')
            _, number_of_rocktes, _ = self.sort_rockets(data, column)
        else:
            number_of_rocktes = number_of_rocktes
        
        colors = cm.gist_rainbow(np.linspace(0.1,0.9,number_of_rocktes))
        
        symbol = ["o","*","^","v",".","<",">","1","2","3","4","8","s","p","P",",","h","H","+","x","X","D","d","|","_"]
        symbol_len = len(symbol)
        
        if number_of_rocktes <= symbol_len:
            symbols = symbol[0:number_of_rocktes]
        else:
            m = int(number_of_rocktes/symbol_len)    
            symbols = symbol*m + symbol[0:(number_of_rocktes-m)]
            
        return colors, symbols