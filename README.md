# SpaceX_read_JSON
This repository contains the Python 3.* library (**extract_data.py**) for extracting data from .json files stored in a SpaceX database [**[1]**](https://github.com/r-spacex/SpaceX-API/wiki/Launches). Furthermore, the repository also contains run script (**run.py**), which is able to extract data from SpaceX database such as rocket name, unix time of start, rocket cargo mass, etc. The only thing we must do is to set the path to the desirable data and have Internet connection or have downloaded .json file with data (in work directory).

# run.py example
In this example we show, how to use extract_data.py library. The script try to download SpaceX past launches to the memory [**[2]**](https://api.spacexdata.com/v2/launches) and store them for the later preparation purposes. If downloading is not possible, the program will take the pre-downloaded (possibility of dealing with old data) file from work directory (past_launches.json). It saves processed data as a **.csv** file and if it is possible as a **.png** figure. 

![IMAGE](https://bitbucket.org/mimi1990/spacex_read_json/downloads/launches_vs_cargo_mass.png)

![IMAGE](https://bitbucket.org/mimi1990/spacex_read_json/downloads/Falcon9_statistics.png)

## Step 1. Load the most important libraries
Whole program uses the standard Python 3.* libraries, but for plotting part it uses Matplotlib and Numpy. The program checks if those extra libraries are installed, if they aren't, program will inform user about that.

## Step 2. Define path to main data
In this part we should define full path through .json file to most diserable data. The path is presented as a list, which should contain key names as a separate strings. If there is more than one value in one key, we should mark this issue, by empty list sign []. The program analyzes this part, and extracts data from every sub-path (only one [] is possible in each main path).

## Step 3. Load the data
The program will creates RJD object from [**[2]**](https://api.spacexdata.com/v2/launches) and stores it as a *json_data* variable. If there is no Internet connection, program will try to upload data from past_launches.json

## Step 4. Extract the data
The program creates RJ object from *json_data* and applys such methods as:

1. Read json_file as a list with every feature from defined paths for every space mission described in downloaded .json file or in past_launches.json.

2. **Filter out** missions **without sucessful start** from *json_data*.

3. **Filter out** missions with **incomplete payload information** from *json_data*.

4. Calculate **total cargo mass** of each non-filtered mission.

5. Convert UNIX datetime to human-readable format *'%Y-%m-%d %H:%M:%S'*

## Step 5. Save as .csv file
Drop information about mission success from data variable, because unsuccessful ones were filtered out. Define labels for .csv file and save them.

## Step 6. Plot the results
Check the system ability in order to plot the data. If it is True:

1. Find *number of rockets* and their *names* in the data variable and store every rocket information from data variable into separate list in *rocket* list. Pro-tip: this method does not require any Numpy or Matplotlib library so if you have properly prepared data  you can use it anywhere.

2. Prepare markers (no matter how many rockets you have), close every plot window from the past and define new window.

3. Plot every mission data as a different plot and set x-axis as date-time

4. EXTRA. Save this plot into work directory (uncomment).

5. For Falcon 9 dataset calculate the yearly total mass of cargo and yearly mean cargo mass with standar deviation. Ps. sending 1 kg of payload to low/mid orbit = ~15000-20000$

6. Plot above data in shared x axis figure and optionally, save it.


[**[1]**](https://github.com/r-spacex/SpaceX-API/wiki/Launches) https://github.com/r-spacex/SpaceX-API/wiki/Launches

[**[2]**](https://api.spacexdata.com/v2/launches) https://api.spacexdata.com/v2/launches