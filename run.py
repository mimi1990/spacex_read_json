#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 12:02:39 2018

@author: Miłosz Martynow
"""
from extract_data import ReadJsonData
from extract_data import ReadJson

from math import sqrt

import datetime as dt

import numpy as np
import matplotlib.pyplot as plt

### DEFINE MOST INTERESTING FEATURES
path_to_content = [
                   ['launch_date_unix'],
                   ['flight_number'],
                   ['launch_success'],
                   ['rocket','rocket_id'],
                   ['rocket','second_stage','payloads',[],'payload_mass_kg']
                  ]

### LOAD THE DATA
RJD = ReadJsonData(default_URL='https://api.spacexdata.com/v2/launches', default_file_name='past_launches.json')
# future -> https://api.spacexdata.com/v2/launches/next
#json_data = RJD.import_json_from_internet(URL='https://api.spacexdata.com/v2/launches') # all OK
#json_data = RJD.import_json_from_file(json_name='past_launches.json') # all OK
json_data = RJD.load_data()

### PREPARE THE DATA
RJ = ReadJson(json_file=json_data, path_to_content=path_to_content)
data = RJ.read_json_files_as_a_list()
data = RJ.filter_by_sucessful_start(data=data)
data = RJ.filter_by_full_mass_information(data=data)
data = RJ.cargo_mass(data=data)
data = RJ.convert_unix(data=data)

### SAVE THE DATA AS CSV FILE
data = RJ.drop_the_colum(data=data, column=2)
labels = ['Date/time of mission', 'Number of mission', 'Rocket name', 'Total cargo mass [kg]']
RJ.save_as_csv(name='launches_vs_cargo_mass.csv',data=data, labels=labels)

### PLOT THE DATA (AND SAVE THE PLOT)
RJ.plot_ability()

names_types, number_of_rocktes, rockets = RJ.sort_rockets(data=data, column=2)
colors, symbols = RJ.colors_and_symbols(number_of_rocktes=number_of_rocktes)

plt.close()
fig, ax = plt.subplots()
for i in range(number_of_rocktes):
    
    dates = RJ.extract_the_column(data=rockets[i], column=0)
    names = RJ.extract_the_column(data=rockets[i], column=2)
    cargo = RJ.extract_the_column(data=rockets[i], column=3)
    
    for j in range(len(dates)):
        dates[j] = dt.datetime.strptime(dates[j], '%Y-%m-%d %H:%M:%S')
    
    col = colors[i]
    sym = symbols[i]
    nam = names[0].capitalize()
    
    ax.plot(dates, cargo, label=nam, c = col, marker = sym, linestyle=':', linewidth=0.5)
    fig.autofmt_xdate()

plt.legend()
plt.xlabel('Date of flight [Year]')
plt.ylabel('Cargo mass [kg]')
plt.grid(True)
plt.show()
#plt.savefig('launches_vs_cargo_mass.png', format='png', dpi=1000)

### PLOT DATA OF FALCON 9 - TOTAL YEARLY CARGO, AVERAGE YEARLY CARGO WITH STANDARD DEVIATION
cargo = RJ.extract_the_column(data=rockets[2], column=3)
dates = RJ.extract_the_column(data=rockets[2], column=0)

for i in range(len(cargo)):
    dates[i] = dt.datetime.strptime(dates[i], '%Y-%m-%d %H:%M:%S') 

# append at the end virtual date and mass - for below algorithm purposes
cargo.append(0)
dates.append(dt.datetime.strptime(str(dates[-1].year+1)+'-01-01 00:00:00', '%Y-%m-%d %H:%M:%S'))

year_cargo = [[dates[0].year, cargo[0]]]
yearly_cargo = []
for i in range(1,len(cargo)):
    
    if dates[i].year == dates[i-1].year:
        year_cargo.append([dates[i].year, cargo[i]])
        
    elif dates[i].year != dates[i-1].year:
        yearly_cargo.append(year_cargo)
        year_cargo = [[dates[i].year, cargo[i]]]

# yearly sum and mean
yearly_mean = []
yearly_sum = []
for i in range(len(yearly_cargo)):
    year_sum = 0
    
    for j in range(len(yearly_cargo[i])):
        year_sum += cargo[j]

    yearly_sum.append(year_sum)        
    yearly_mean.append(year_sum/len(yearly_cargo[i]))   

# yearly standard deviation
yearly_std = []
for i in range(len(yearly_cargo)):
    anomaly = 0
    
    for j in range(len(yearly_cargo[i])):
        anomaly += (yearly_cargo[i][j][1]-yearly_mean[i])**2

    yearly_std.append(sqrt(anomaly/len(yearly_cargo[i])))

# set years vector
year_beg = dates[0].year
year_end = dates[-1].year
years = []
for y in range(year_beg,year_end):
    years.append(y)
    
std_minus = []
std_plus = []

for i in range(len(yearly_mean)):
    std_minus.append(yearly_mean[i]-yearly_std[i])
    std_plus.append(yearly_mean[i]+yearly_std[i])
    
f, (ax1, ax2) = plt.subplots(2, sharex=True, sharey=False)

ax1.plot(years, yearly_sum, c = colors[2], marker = symbols[2], linestyle=':', linewidth=0.5, label='Yearly total cargo mass')
ax1.set_title('Exploatation of Falcon 9 rocket')
ax1.set_ylabel('[kg]')
ax1.legend(loc='upper left')
ax1.grid()

ax2.fill_between(years, std_minus, std_plus,
    alpha=0.5, edgecolor='m', facecolor='m',label='Standard deviation')
ax2.plot(years, yearly_mean, c = colors[2], marker = symbols[2], linestyle=':', linewidth=0.5, label='Yearly mean cargo mass ')
ax2.set_ylabel('[kg]')
ax2.legend(loc='upper left')
ax2.grid()

f.subplots_adjust(hspace=0)
plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
plt.show()
#plt.savefig('Falcon9_statistics.png', format='png', dpi=1000)